# README #

**This repository has been rebuilt from it's initial 2019 version due to data corrpution - please ignore the commit timestamps**

### What is this repository for? ###

* This repository provides the installable .apk file of VERITAS - a proof of concept mind mapping application for the Oculus Go Virtual Reality headset.  It can be sideloaded to the device (see online guides for details on ow to do this).
* Version 1

### How do I get set up? ###

* Side load the application on your Oculus Go Headset
* Start the VERITAS application once and exit immediately
* Plug the headset into a computer
* On the device, navigate to \Android\data\com.LancasterUni.Veritas\files
* You will see a config.json file - edit this file to use your own images/text for the tiles, enable or disable the tutorial and post activity tasks, and to enable logging
* If logging is enabled in the config file, the log files will be written to the above folder.  Two log files are written - one that records interactions and one that provides tile positions.

### Contribution guidelines ###

* While the source code is closed, you can report bugs as per normal .git processes or by email the contact below.

### Who do I talk to? ###

* Rob Sims, r.sims2@lancaster.ac.uk
